from abc import ABCMeta, abstractmethod


class ExperimentConfiguration(object):

    __metaclass__ = ABCMeta

    def __init__(self, experiment_id=0, run_id=0):
        """
        :type experiment_id: int
        :type run_id: int
        """
        self.experiment_id = experiment_id
        self.run_id = run_id

    @classmethod
    @abstractmethod
    def from_json(cls, json):
        return cls(**json)
