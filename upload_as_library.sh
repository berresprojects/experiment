#!/bin/bash

LIBRARY_LOCATION='/home/pi/.local/lib/python2.7/site-packages/experiment/'

ssh $1 'mkdir -p '"$LIBRARY_LOCATION"
ssh $1 'rm -rf '"${LIBRARY_LOCATION}"'/*'
scp -r ./tests ./experiment.py ./experiment_configuration.py __init__.py $1:${LIBRARY_LOCATION}
