from unittest import TestCase

from mock import MagicMock

from experiment import Experiment
from experiment_configuration import ExperimentConfiguration


class ConcreteExperiment(Experiment):

    def can_perform_experiment_step(self):
        pass

    def perform_experiment_step(self):
        pass


class TestExperiment(TestCase):

    def test_pre_loop_action(self):
        experiment_configuration = ExperimentConfiguration()
        experiment = ConcreteExperiment(experiment_configuration)
        experiment.can_perform_experiment_step = MagicMock()
        experiment.perform_experiment_step = MagicMock()
        experiment.prepare_experiment = MagicMock()

        experiment.pre_loop_action()

        experiment.prepare_experiment.assert_called_once()

    def test_loop_action(self):
        experiment_configuration = ExperimentConfiguration()
        experiment = ConcreteExperiment(experiment_configuration)
        experiment.can_perform_experiment_step = MagicMock()
        experiment.perform_experiment_step = MagicMock()
        experiment.prepare_experiment = MagicMock()

        experiment.loop_action()

        experiment.can_perform_experiment_step.assert_called_once()
        experiment.perform_experiment_step.assert_called_once()
