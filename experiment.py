from abc import abstractmethod, ABCMeta
from logging import getLogger
from time import sleep

from looping_thread.looping_thread import LoopingThread

from experiment_configuration import ExperimentConfiguration

logger = getLogger()


class Experiment(LoopingThread):

    __metaclass__ = ABCMeta

    def __init__(self, experiment_configuration):
        """
        :type experiment_configuration: experiment_configuration.ExperimentConfiguration
        """
        super(Experiment, self).__init__()
        self.__experiment_configuration = experiment_configuration
        self.__experiment_step_in_progress = False
        self.__experiment_step = 1
        self.experiment_is_prepared = False

    @abstractmethod
    def perform_experiment_step(self):
        pass

    @abstractmethod
    def can_perform_experiment_step(self):
        """
        :rtype: bool
        """
        pass

    def prepare_experiment(self):
        """
        :rtype: bool
        """
        return True

    def pre_loop_action(self):
        self.experiment_is_prepared = self.prepare_experiment()

    def loop_action(self):
        if self.can_perform_experiment_step():
            self.__experiment_step_in_progress = True
            self.perform_experiment_step()
            self.__experiment_step += 1
            self.__experiment_step_in_progress = False

    def get_experiment_configuration(self):
        """
        :rtype: ExperimentConfiguration
        """
        return self.__experiment_configuration

    def get_experiment_step(self):
        return self.__experiment_step

    def destroy(self):
        super(Experiment, self).destroy()
        # Wait for last run to complete
        while self.__experiment_step_in_progress:
            sleep(0.00001)
